# step by step instructions to create a docker image

# starts with a base image
#1. OS
#2. JDK
#3. copy the code
#4. mvn clean package
#5. run the jar file

# write the instructions to build an image
# Docker image can be constructed using predefined commands

#base image
FROM openjdk:11-jdk-slim as builder

#setup the working directory
WORKDIR /app

#copy the mvn files from the host to the image directly
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .

#give the mvn the executable permission
RUN chmod +x ./mvnw && ./mvnw -B dependency:go-offline

#copy the source code
COPY src src
RUN ./mvnw package -DskipTests

RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)


# create a brand new image and copy the output from the base image to this new image
FROM openjdk:11.0.13-jre-slim-buster as stage

#argument
ARG DEPENDENCY=/app/target/dependency
ENV SPRING_PROFILE_ACTIVE=dev

# Copy the dependency application file from builder stage artifact
COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=builder ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /app

EXPOSE 8222

ENTRYPOINT ["java", "-cp", "app:app/lib/*", "com.classpath.orderserviceapi.OrderServiceApiApplication"]


