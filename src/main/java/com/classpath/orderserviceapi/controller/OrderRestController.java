package com.classpath.orderserviceapi.controller;

import com.classpath.orderserviceapi.model.Order;
import com.classpath.orderserviceapi.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/orders")
@Slf4j

public class OrderRestController {
    private final OrderService orderService;

    public OrderRestController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping
    @Operation(description = "API to fetch all the orders")
    public Map<String, Object> fetchAllOrders(
            @Parameter(name = "page", required = true,example = "1", allowEmptyValue = false)
            @RequestParam(name="page", required = false, defaultValue = "0") int page,
            @RequestParam(name="size", required = false, defaultValue = "10") int size,
            @RequestParam(name="order", required = false, defaultValue = "asc") String direction,
            @RequestParam(name="field", required = false, defaultValue = "name") String property){

        return this.orderService.fetchOrders(page, size, direction, property);
    }

    @GetMapping("/price")
    public Map<String, Object> fetchAllOrdersByPriceRange(
            @RequestParam(name="min", required = false, defaultValue = "0") double min,
            @RequestParam(name="max", required = false, defaultValue = "10") double max){

        log.info("Min value : {}", min);
        log.info("Max value : {}", max);

        return this.orderService.findByPriceRange(min, max);
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable("id") long orderId){
        return this.orderService.findOrderById(orderId);
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Order saveOrder(@RequestBody @Valid Order order){
        return this.orderService.saveOrder(order);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderById(@PathVariable("id") long orderId){
        this.orderService.deleteOrder(orderId);
    }
}
