package com.classpath.orderserviceapi.util;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import java.util.Arrays;

@Configuration
public class ApplicationContextRunner  implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    public ApplicationContextRunner(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void run(String... args) throws Exception {
        String[] beanDefinitionNames = this.applicationContext.getBeanDefinitionNames();
        Arrays.asList(beanDefinitionNames)
                .stream()
                .filter(bean -> bean.startsWith("user"))
                .forEach(System.out::println);
    }
}
