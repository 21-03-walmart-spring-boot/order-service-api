package com.classpath.orderserviceapi.service;

import com.classpath.orderserviceapi.model.Order;
import com.classpath.orderserviceapi.repository.OrderRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }


    public Order saveOrder(Order order){
        return this.orderRepository.save(order);
    }

    public Map<String, Object> fetchOrders(int page, int size, String direction, String property){
        Sort.Direction sortDirection = direction.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        PageRequest pageRequest = PageRequest.of(page, size, sortDirection, property);
        Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);

        long totalRecords = pageResponse.getTotalElements();
        int pages = pageResponse.getTotalPages();
        int pageNumber = pageResponse.getNumber();
        int recordsPerPage = pageResponse.getSize();
        List<Order> records = pageResponse.getContent();

        LinkedHashMap<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("total-records", totalRecords);
        responseMap.put("total-pages", pages);
        responseMap.put("page", pageNumber);
        responseMap.put("records", recordsPerPage);
        responseMap.put("data", records);

        return responseMap;
    }

    public Order findOrderById(long orderId){
        return this.orderRepository
                .findById(orderId)
                .orElseThrow(() -> new IllegalArgumentException("invalid order id"));
    }

    public void deleteOrder(long orderId){
        this.orderRepository.deleteById(orderId);
    }

    public Order updateOrderById(long orderId, Order updatedOrder){
        this.orderRepository.findById(orderId)
                .stream().map(order -> updatedOrder)
                .map(orderToBeUpdated -> this.orderRepository.save(orderToBeUpdated))
                .forEach(savedOrder -> System.out.println(" Order is updated"));
        return updatedOrder;
    }

    public Map<String, Object> findByPriceRange(double min, double max){
        Pageable pageRequest = PageRequest.of(0, 10, Sort.Direction.ASC, "name" );

        Page<Order> pageResponse = this.orderRepository.findByPriceBetween(min, max, pageRequest);


        long totalRecords = pageResponse.getTotalElements();
        int pages = pageResponse.getTotalPages();
        int pageNumber = pageResponse.getNumber();
        int recordsPerPage = pageResponse.getSize();
        List<Order> records = pageResponse.getContent();

        LinkedHashMap<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put("total-records", totalRecords);
        responseMap.put("total-pages", pages);
        responseMap.put("page", pageNumber);
        responseMap.put("records", recordsPerPage);
        responseMap.put("data", records);
        return responseMap;
    }
}
