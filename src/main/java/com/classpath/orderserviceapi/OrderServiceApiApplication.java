package com.classpath.orderserviceapi;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(
      info = @Info(
              title = "Orders Service",
              description = "API for accessing orders",
              contact = @Contact(
                      email = "developer@gmail.com",
                      url = "abc.com"
              )
              ,version = "1.0.0"

      ) ,
        servers = @Server(url = "http://localhost:8555")
)
public class OrderServiceApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderServiceApiApplication.class, args);
    }

}
