package com.classpath.orderserviceapi.event;

import com.classpath.orderserviceapi.model.LineItem;
import com.classpath.orderserviceapi.model.Order;
import com.classpath.orderserviceapi.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Component
@Slf4j
@RequiredArgsConstructor
@Profile({"dev", "qa"})
public class BootstrapAppData {

    private final OrderRepository orderRepository;
    private final Faker faker = new Faker();
    private final Environment environment;

    @Value("${app.orders.limit}")
    private int limit;

    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationLoad(ApplicationReadyEvent event){
      log.info("========== Application is ready  in ========== "+ this.environment.getActiveProfiles()[0]);
        Set<Order> orders = new HashSet <>();
        IntStream.range(0, 200).forEach(index -> {
            Order order = Order
                            .builder()
                                .name(faker.name().firstName())
                                .price(faker.number().randomDouble(2, 6000, 10_000))
                                .orderDate(faker.date().past(8, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                            .build();
            IntStream.range(1, faker.number().numberBetween(2, 8)).forEach(loop -> {
                LineItem lineItem = LineItem.builder()
                                                .name(faker.commerce().productName())
                                                .qty(faker.number().numberBetween(2 , 6))
                                                .build();
                  order.addLineItem(lineItem);
                  orders.add(order);
                  this.orderRepository.save(order);
            });

        });
    }
}
