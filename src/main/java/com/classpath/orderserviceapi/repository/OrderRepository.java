package com.classpath.orderserviceapi.repository;

import com.classpath.orderserviceapi.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findByPriceLessThan(double price);

    Page<Order> findByPriceBetween(double min, double max, Pageable pageRequest);

    @Query("select order from Order order where order.name = ?1")
    Order findByCustomerName(String customerName);

}
