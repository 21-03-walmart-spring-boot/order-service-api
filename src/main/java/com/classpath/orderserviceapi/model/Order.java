package com.classpath.orderserviceapi.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor

@Entity
@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Min(value = 5000, message = "Min order price should  be 5k")
    @Max(value = 10000, message = "Max order price should not exceed 10k")
    private double price;

    @NotBlank(message = "Name cannot be blank")
    private String name;

    @PastOrPresent(message = "Order date cannot be in the past")
    private LocalDate orderDate;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonManagedReference
    private Set<LineItem> lineItems;

    public void addLineItem(LineItem lineItem){
        this.lineItems = this.lineItems == null ? new HashSet<>(): this.lineItems;
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }
}
