package com.classpath.orderserviceapi.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = "app", value = "loadUserBean", havingValue = "true", matchIfMissing = true)
    public User userBean(){
        return new User();
    }
    @Bean
    @ConditionalOnBean(name = "userBean")
    public User userBeanOnBeanCondition(){
        return new User();
    }
    @Bean
    @ConditionalOnMissingBean(name = "userBean")
    public User userBeanOnMissingBeanCondition(){
        return new User();
    }
    @Bean
    @ConditionalOnMissingClass(value = "x.y.Z")
    public User userBeanOnMissingClassCondition(){
        return new User();
    }

}

class User{

}
