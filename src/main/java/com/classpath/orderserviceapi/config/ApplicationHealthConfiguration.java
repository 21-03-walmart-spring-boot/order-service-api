package com.classpath.orderserviceapi.config;

import com.classpath.orderserviceapi.repository.OrderRepository;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class ApplicationHealthConfiguration implements HealthIndicator {
    private final OrderRepository orderRepository;

    public ApplicationHealthConfiguration(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Health health() {
        try {
            long count = this.orderRepository.count();
            return Health.up().withDetail("DB Service", "Service is healthy").build();
        }catch (Exception exception){
            return Health.down().withDetail("DB Service", "Service is unhealthy").build();
        }
    }
}
